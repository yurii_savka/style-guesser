package com.styleguesser.ml.util;


import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.util.List;

public class TrainData {
    List<ExtractedFeatures> featuresList;
    List<String> fields;
    List<String> classes;

    public List<ExtractedFeatures> getFeaturesList() {
        return featuresList;
    }

    public void setFeaturesList(List<ExtractedFeatures> featuresList) {
        this.featuresList = featuresList;
    }

    public List<String> getFields() {
        return fields;
    }

    public void setFields(List<String> fields) {
        this.fields = fields;
    }

    public List<String> getClasses() {
        return classes;
    }

    public void setClasses(List<String> classes) {
        this.classes = classes;
    }
}
