package com.styleguesser.ml.util;

import java.util.Map;

public class GuesserResult {
    public Map<String, Double> result;

    public GuesserResult(Map<String, Double> result) {
        this.result = result;
    }
    public GuesserResult() {}
}
