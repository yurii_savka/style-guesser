package com.styleguesser.ml.util;

import java.util.List;
import java.util.Set;

public interface FeatureExtractor {
    ExtractedFeatures extractFeatures(TextWithAuthor textWithAuthor) throws Exception;

    TrainData getTrainData(List<TextWithAuthor> texts, List<String> classes) throws Exception;
    void setFields(Set<String> fields);
}
