package com.styleguesser.ml.util;

import weka.classifiers.AbstractClassifier;
import weka.classifiers.Classifier;
import weka.classifiers.bayes.BayesNet;
import weka.classifiers.bayes.NaiveBayes;
import weka.classifiers.functions.MultilayerPerceptron;
import weka.core.Attribute;
import weka.core.DenseInstance;
import weka.core.Instance;
import weka.core.Instances;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class ModelBuilder {
    public final static int MODE_BAYES = 0;
    public final static int MODE_MULTILAYER_PERCEPTRON = 1;
    public final static int MODE_BAYES_NET = 2;

    public final static String PERCEPTRON_LAYERS = "10,10";

    private ArrayList<Attribute> fvWekaAttributes;
    AbstractClassifier classifier;
    Instances isTrainingSet;
    List<String> fields;
    public int mode = MODE_MULTILAYER_PERCEPTRON;

    public void initialize(TrainData trainData) {
        initialize(trainData.getFields(), trainData.getClasses());
    }

    public void initialize(List<String> fields, List<String> classes) {
        this.fields = fields;
        ArrayList<String> fvClassVal = new ArrayList<>(classes.size());
        fvClassVal.addAll(classes.stream().collect(Collectors.toList()));

        fvWekaAttributes = new ArrayList<>(fields.size() + 1);
        fvWekaAttributes.add(new Attribute("author", fvClassVal));
        Attribute attr;
        for (String field : fields) {
            attr = new Attribute(field);

            fvWekaAttributes.add(attr);
        }
        // Only needed because of specific implementation of weka.
        new Instances("init", fvWekaAttributes, 1);
    }

    private AbstractClassifier getClassifier() {
        switch (mode) {
            case MODE_BAYES:
                return new NaiveBayes();
            case MODE_MULTILAYER_PERCEPTRON:
                MultilayerPerceptron ml = new MultilayerPerceptron();
                ml.setHiddenLayers(PERCEPTRON_LAYERS);
                return ml;
            case MODE_BAYES_NET:
                return new BayesNet();
            default:
                return new NaiveBayes();
        }
    }

    private Instance getInstance(ExtractedFeatures features) {
        Instance iExample = new DenseInstance(fvWekaAttributes.size());
        if (!features.getAuthor().equals(ExtractedFeatures.AUTHOR_UNKNOWN)) {
            iExample.setValue(fvWekaAttributes.get(0), features.getAuthor());
        }

        int i = 1;
        for (String field : fields) {
            iExample.setValue(fvWekaAttributes.get(i++), features.get(field));
        }

        return iExample;
    }

    public AbstractClassifier train(TrainData trainData) throws Exception {
        initialize(trainData);

        List<ExtractedFeatures> featuresList = trainData.getFeaturesList();
        isTrainingSet = new Instances("Training", fvWekaAttributes, featuresList.size());
        isTrainingSet.setClassIndex(0);

        for (ExtractedFeatures features : featuresList) {
            isTrainingSet.add(getInstance(features));
        }

        classifier = getClassifier();
        classifier.buildClassifier(isTrainingSet);

        return classifier;
    }

    public double[] evaluate(ExtractedFeatures features, Classifier classifier) throws Exception {
        Instance instance = getInstance(features);

        return classifier.distributionForInstance(instance);
    }
}
