package com.styleguesser.ml.util;

import weka.core.tokenizers.CharacterNGramTokenizer;

import java.util.*;
import java.util.stream.Collectors;

public class CharacterNGramFeatureExtractor implements FeatureExtractor {
    public static final int NGRAM_SIZE = 3;
    public static final int NGRAM_TOP_COUNT = 1000;
    Set <String> fields;
    CharacterNGramTokenizer tokenizer;

    public CharacterNGramFeatureExtractor() {
        tokenizer = new CharacterNGramTokenizer();
    }

    class ValueComparator implements Comparator<String> {
        Map<String, Integer> base;

        public ValueComparator(Map<String, Integer> base) {
            this.base = base;
        }

        public int compare(String a, String b) {
            return base.get(a) >= base.get(b) ? -1 : 1;
        }
    }

    class NGramContainer {
        String author;
        Map<String, Integer> nGrams;
        long textSize;

        public NGramContainer(String author, Map<String, Integer> nGrams, long textSize) {
            this.author = author;
            this.nGrams = nGrams;
            this.textSize = textSize;
        }

        public String getAuthor() {
            return author;
        }

        public Map<String, Integer> getNGrams() {
            return nGrams;
        }

        public long getTextSize() {
            return textSize;
        }
    }

    public ExtractedFeatures extractFeatures(TextWithAuthor textWithAuthor) throws Exception {
        if (fields.isEmpty()) {
            throw new Exception("NGrams are not defined");
        }
        String text = filterText(textWithAuthor.getText());
        Map<String, Integer> textNGrams = new HashMap<>();
        tokenizer.tokenize(text);
        while (tokenizer.hasMoreElements()) {
            String nGram = tokenizer.nextElement().toLowerCase();

            Integer textNGramCount = textNGrams.get(nGram);
            textNGrams.put(nGram, textNGramCount != null ? textNGramCount + 1 : 1);
        }

        return extractFeatures(new NGramContainer(textWithAuthor.getAuthor(), textNGrams, text.length()));
    }

    public ExtractedFeatures extractFeatures(NGramContainer container) {
        ExtractedFeatures features = new ExtractedFeatures();
        features.setAuthor(container.getAuthor());
        Map<String, Integer> textNGrams = container.getNGrams();
        for (String topNGram : fields) {
            Integer textNGramCount = textNGrams.get(topNGram);
            if (textNGramCount == null) {
                features.set(topNGram, 0);
            } else {
                features.set(topNGram, (double) textNGramCount / container.getTextSize());
            }
        }

        return features;
    }

    private String filterText(String text) {
        text = text.replace('\n', ' ');
        text = text.replace('\r', ' ');
        text = text.replace("   ", " ");
        text = text.replace("  ", " ");

        return text;
    }

    private void calculateTopN(Map<String, Integer> nGrams) {
        ValueComparator bvc = new ValueComparator(nGrams);
        SortedMap<String, Integer> topNGramsTree = new TreeMap<>(bvc);
        topNGramsTree.putAll(nGrams);
        int i = 0;
        String toKey = "";
        for (String key : topNGramsTree.keySet()) {
            if (i++ == NGRAM_TOP_COUNT) {
                toKey = key;
                break;
            }
        }

        fields = topNGramsTree.headMap(toKey).keySet();
    }

    public void setFields(Set<String> fields) {
        this.fields = fields;
    }

    public TrainData getTrainData(List<TextWithAuthor> textsWithAuthors, List<String> classes) throws Exception {
        Map<String, Integer> nGrams = new TreeMap<>();
        List<NGramContainer> containers = new ArrayList<>();

        tokenizer.setNGramMinSize(NGRAM_SIZE);
        tokenizer.setNGramMaxSize(NGRAM_SIZE);

        for (TextWithAuthor textWithAuthor : textsWithAuthors) {
            String text = filterText(textWithAuthor.getText());
            Map<String, Integer> textNGrams = new HashMap<>();
            tokenizer.tokenize(text);
            while (tokenizer.hasMoreElements()) {
                String nGram = tokenizer.nextElement().toLowerCase();
                Integer nGramCount = nGrams.get(nGram);
                nGrams.put(nGram, nGramCount != null ? nGramCount + 1 : 1);

                Integer textNGramCount = textNGrams.get(nGram);
                textNGrams.put(nGram, textNGramCount != null ? textNGramCount + 1 : 1);
            }
            containers.add(new NGramContainer(textWithAuthor.getAuthor(), textNGrams, text.length()));
        }

        calculateTopN(nGrams);

        TrainData tData = new TrainData();
        tData.setClasses(classes);
        tData.setFields(new ArrayList<>(fields));

        List<ExtractedFeatures> featuresList = containers
                .stream()
                .map(this::extractFeatures)
                .collect(Collectors.toList());

        tData.setFeaturesList(featuresList);

        return tData;
    }
}
