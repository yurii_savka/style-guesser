package com.styleguesser.ml.util;


import com.fasterxml.jackson.databind.ObjectMapper;
import weka.classifiers.AbstractClassifier;
import weka.classifiers.Classifier;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class Persister {
    public static final String MODEL_PATH = "data/trained/mlp.model";
    public static final String FIELDS_PATH = "data/trained/fields.json";
    public static final String CLASSES_PATH = "data/trained/classes.json";

    ObjectMapper mapper;

    public Persister() {
        this.mapper = new ObjectMapper();
    }

    public void persistClassifier(AbstractClassifier classifier) throws Exception {
        weka.core.SerializationHelper.write(MODEL_PATH, classifier);
    }

    public Classifier readClassifier() throws Exception {
        return (Classifier) weka.core.SerializationHelper.read(MODEL_PATH);
    }

    public static class FieldsWrapper {
        List<String> fields;

        public FieldsWrapper() {
        }

        public FieldsWrapper(List<String> fields) {
            this.fields = fields;
        }

        public List<String> getFields() {
            return fields;
        }
    }

    public static class ClassesWrapper {
        String[] classes;

        public ClassesWrapper() {
        }

        public ClassesWrapper(String[] classes) {
            this.classes = classes;
        }

        public String[] getClasses() {
            return classes;
        }
    }

    public void persistFields(List<String> fields) throws Exception {
        mapper.writeValue(new File(FIELDS_PATH), new FieldsWrapper(fields));
    }

    public List<String> readFields() throws IOException {
        FieldsWrapper fields = mapper.readValue(new File(FIELDS_PATH), FieldsWrapper.class);

        return fields.getFields();
    }

    public void persistCLasses(String[] classes) throws IOException {
        mapper.writeValue(new File(CLASSES_PATH), new ClassesWrapper(classes));
    }

    public String[] readClasses() throws IOException {
        ClassesWrapper classes = mapper.readValue(new File(CLASSES_PATH), ClassesWrapper.class);

        return classes.getClasses();
    }
}
