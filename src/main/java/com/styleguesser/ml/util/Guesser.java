package com.styleguesser.ml.util;

import weka.classifiers.Classifier;

import java.util.*;

public class Guesser {
    ModelBuilder modelBuilder;
    FeatureExtractor extractor;
    Classifier classifier;
    String[] classes;

    private void initialize() {
        modelBuilder = new ModelBuilder();
        extractor = new CharacterNGramFeatureExtractor();

        try {
            Persister persister = new Persister();
            classifier = persister.readClassifier();
            List<String> fields = persister.readFields();
            extractor.setFields(new HashSet<>(fields));
            classes = persister.readClasses();
            modelBuilder.initialize(fields, Arrays.asList(classes));
        } catch (Exception e) {
            // @todo: how should we handle exceptions???
        }
    }

    public Map<String, Double> guess(String content) throws Exception {
        if (modelBuilder == null) {
            initialize();
        }
        double[] vector = modelBuilder.evaluate(extractor.extractFeatures(new TextWithAuthor(content, "X")), classifier);
        Map<String, Double> result = new HashMap<>();
        if (vector.length != classes.length) {
            throw new Exception("Classes/vector length mismatch!");
        }
        for (int i = 0; i < vector.length; i++) {
            result.put(classes[i], vector[i]);
        }

        return result;
    }

    public String filterText(String text) {
        text = text.replace('\n', ' ');
        text = text.replace('\r', ' ');
        text = text.replace("   ", " ");
        text = text.replace("  ", " ");
        text = text.replace("  ", " ");
        text = text.replace("  ", " ");

        return text;
    }
}
