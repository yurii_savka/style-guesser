package com.styleguesser.ml;

import com.styleguesser.ml.util.*;
import weka.classifiers.Classifier;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;

public class GuesserApp {
    public static void main(String[] args) throws Exception {
        ModelBuilder modelBuilder = new ModelBuilder();
        FeatureExtractor extractor = new CharacterNGramFeatureExtractor();

        Persister persister = new Persister();
        Classifier classifier = persister.readClassifier();
        List<String> fields = persister.readFields();
        extractor.setFields(new HashSet<>(fields));
        String[] classes = persister.readClasses();
        modelBuilder.initialize(fields, Arrays.asList(classes));

        for (String cls : classes) {
            String testContent = new String(Files.readAllBytes(Paths.get("data/" + cls + "/test.txt")));
            double[] result = modelBuilder.evaluate(extractor.extractFeatures(new TextWithAuthor(testContent, "X")), classifier);
            System.out.println("Real: " + cls + " Prediction: " + "Classes:" + Arrays.toString(classes) + Arrays.toString(result));
        }

        String testContent = new String(Files.readAllBytes(Paths.get("data/test.txt")));
        double[] result = modelBuilder.evaluate(extractor.extractFeatures(new TextWithAuthor(testContent, "X")), classifier);
        System.out.println("Real: ? Prediction: Classes:" + Arrays.toString(classes) + Arrays.toString(result));
    }
}
