package com.styleguesser.ml;

import com.styleguesser.ml.util.*;
import weka.classifiers.AbstractClassifier;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class TrainerApp {

    public static void main(String[] args) throws Exception {
        FeatureExtractor extractor = new CharacterNGramFeatureExtractor();
        String[] classes = {"Zhadan", "Zabuzhko", "LesUkr", "HP"};
        List<TextWithAuthor> input = new ArrayList<>();
        for (String cls : classes) {
            for (int i = 1; i < 5; i++) {
                String content = new String(Files.readAllBytes(Paths.get("data/" + cls + "/" + i + ".txt")));
                input.add((new TextWithAuthor(content, cls)));
            }
        }

        ModelBuilder modelBuilder = new ModelBuilder();
        TrainData tData = extractor.getTrainData(input, Arrays.asList(classes));
        AbstractClassifier classifier = modelBuilder.train(tData);

        Persister persister = new Persister();
        persister.persistClassifier(classifier);
        persister.persistFields(tData.getFields());
        persister.persistCLasses(classes);

        for (String cls : classes) {
            String testContent = new String(Files.readAllBytes(Paths.get("data/" + cls + "/test.txt")));
            double[] result = modelBuilder.evaluate(extractor.extractFeatures(new TextWithAuthor(testContent, "X")), classifier);
            System.out.println("Real: " + cls + " Prediction: " + "Classes:" + Arrays.toString(classes) + Arrays.toString(result));
        }

        String testContent = new String(Files.readAllBytes(Paths.get("data/test.txt")));
        double[] result = modelBuilder.evaluate(extractor.extractFeatures(new TextWithAuthor(testContent, "X")), classifier);
        System.out.println("Real: ? Prediction: Classes:" + Arrays.toString(classes) + Arrays.toString(result));
    }
}
