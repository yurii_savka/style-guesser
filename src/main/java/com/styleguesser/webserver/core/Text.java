package com.styleguesser.webserver.core;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "texts")
@NamedQueries(
    {
        @NamedQuery(
            name = "com.style-guesser.webserver.core.Text.findAll",
            query = "SELECT t FROM Text t"
        )
    }
)
public class Text {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "content", nullable = false)
    private String content;

    public Text() {
    }

    public Text(String content) {
        this.content = content;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Text)) {
            return false;
        }

        final Text that = (Text) o;

        return Objects.equals(this.id, that.id) &&
                Objects.equals(this.content, that.content);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, content);
    }
}
