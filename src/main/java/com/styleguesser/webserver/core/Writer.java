package com.styleguesser.webserver.core;

import javax.persistence.*;
import java.util.Objects;

@Entity
@Table(name = "writers")
@NamedQueries(
    {
        @NamedQuery(
            name = "com.style-guesser.webserver.core.Writer.findAll",
            query = "SELECT w FROM Writer w"
        )
    }
)
public class Writer {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "fullName", nullable = false)
    private String fullName;

    @Column(name = "description", nullable = false)
    private String description;

    public Writer() {
    }

    public Writer(String fullName, String description) {
        this.fullName = fullName;
        this.description = description;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public String getFullName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Writer)) {
            return false;
        }

        final Writer that = (Writer) o;

        return Objects.equals(this.id, that.id) &&
                Objects.equals(this.fullName, that.fullName) &&
                Objects.equals(this.description, that.description);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, fullName, description);
    }
}
