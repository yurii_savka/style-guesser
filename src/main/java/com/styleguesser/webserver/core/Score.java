package com.styleguesser.webserver.core;

import javax.persistence.*;
import java.text.DecimalFormat;
import java.util.Objects;

@Entity
@Table(name = "scores")
@NamedQueries(
    {
        @NamedQuery(
            name = "com.style-guesser.webserver.core.Score.findAll",
            query = "SELECT s FROM Score s"
        ),
        @NamedQuery(
            name="com.style-guesser.webserver.core.Score.findByTextId",
            query="SELECT s FROM Score s WHERE s.textId = :textId"
        )
    }
)
public class Score {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    @Column(name = "textId", nullable = false)
    private long textId;

    @Column(name = "authorKey", nullable = false)
    private String authorKey;

    @Column(name = "score", nullable = false)
    private double score;

    public Score() {
    }

    public Score(String authorId, long textId, double score) {
        this.authorKey = authorId;
        this.textId = textId;
        this.score = score;
    }

    public long getId() {
        return id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getTextId() {
        return textId;
    }

    public void setTextId(long textId) {
        this.textId = textId;
    }

    public double getScore() {
        return score;
    }

    public String getScoreFormatted() {
        return new DecimalFormat("#.##").format(score);
    }

    public long getScorePercentage() {
        return (long) (score * 100);
    }

    public void setScore(double score) {
        this.score = score;
    }

    public String getAuthorKey() {
        return authorKey;
    }

    public void setAuthorKey(String authorKey) {
        this.authorKey = authorKey;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Score)) {
            return false;
        }

        final Score that = (Score) o;

        return Objects.equals(this.id, that.id) &&
                Objects.equals(this.authorKey, that.authorKey) &&
                Objects.equals(this.score, that.score);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, authorKey, score);
    }
}
