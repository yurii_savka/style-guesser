package com.styleguesser.webserver.resources;

import com.styleguesser.webserver.core.Writer;
import com.styleguesser.webserver.db.WriterDAO;
import com.styleguesser.webserver.views.WriterFormView;
import io.dropwizard.hibernate.UnitOfWork;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import java.net.URI;
import java.util.List;

@Path("/writers")
@Produces(MediaType.APPLICATION_JSON)
public class WritersResource {

    private final WriterDAO writersDAO;

    public WritersResource(WriterDAO writersDAO) {
        this.writersDAO = writersDAO;
    }

    @POST
    @UnitOfWork
    public Writer createWriter(Writer Writer) {
        return writersDAO.create(Writer);
    }

    @POST
    @UnitOfWork
    @Path("/add")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public Response createWriterFromForm(@FormParam("fullName") String fullName, @FormParam("description") String description) {
        Writer writer = new Writer();
        writer.setFullName(fullName);
        writer.setDescription(description);
        writersDAO.create(writer);
        URI uri = UriBuilder.fromUri("/writers").build();
        return Response.seeOther(uri).build();
    }

    @GET
    @Path("/form")
    @UnitOfWork
    @Produces(MediaType.TEXT_HTML)
    public WriterFormView getWriterFormView() {
        return new WriterFormView();
    }

    @GET
    @UnitOfWork
    public List<Writer> listWriters() {
        return writersDAO.findAll();
    }

}