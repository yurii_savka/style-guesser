package com.styleguesser.webserver.resources;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.styleguesser.ml.util.Guesser;
import com.styleguesser.webserver.core.Score;
import com.styleguesser.webserver.core.Text;
import com.styleguesser.webserver.db.ScoreDAO;
import com.styleguesser.webserver.db.TextDAO;
import com.styleguesser.webserver.views.ResultView;
import com.styleguesser.webserver.views.TextFormView;
import io.dropwizard.hibernate.UnitOfWork;
import io.dropwizard.jersey.params.LongParam;
import org.apache.commons.lang3.StringEscapeUtils;

import javax.ws.rs.*;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import java.io.IOException;
import java.net.URI;
import java.util.List;
import java.util.Map;

@Path("/texts")
@Produces(MediaType.APPLICATION_JSON)
public class TextResource {

    private final TextDAO textsDAO;
    private final ScoreDAO scoreDAO;

    public TextResource(TextDAO textsDAO, ScoreDAO scoreDAO) {
        this.textsDAO = textsDAO;
        this.scoreDAO = scoreDAO;
    }

    @POST
    @UnitOfWork
    public Text createText(Text Text) {
        return textsDAO.create(Text);
    }

    @POST
    @UnitOfWork
    @Path("/add")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public Response createTextFromForm(@FormParam("content") String content) {
        Injector injector = Guice.createInjector();
        Guesser guesser = injector.getInstance(Guesser.class);

        content = guesser.filterText(content);
        content = StringEscapeUtils.unescapeHtml4(content);
        Text text = new Text();
        text.setContent(content);
        Text newText = textsDAO.create(text);
        try {
            Map<String, Double> vector = guesser.guess(content);
            vector.forEach((authorKey,score) -> {
                Score s = new Score(authorKey, newText.getId(), score);
                scoreDAO.create(s);
            });

        } catch (Exception e) {
            // Redirect to error here
        }
        URI uri = UriBuilder.fromUri("/texts/" + newText.getId()).build();
        return Response.seeOther(uri).build();
    }

    @GET
    @Path("/form")
    @UnitOfWork
    @Produces(MediaType.TEXT_HTML)
    public TextFormView getTextFormView() {
        return new TextFormView();
    }

    @GET
    @UnitOfWork
    public List<Text> listTexts() {
        return textsDAO.findAll();
    }

    @GET
    @Path("/{id}")
    @UnitOfWork
    @Produces(MediaType.TEXT_HTML)
    public ResultView getText(@PathParam("id") LongParam id) throws IOException {
        Text text = findSafely(id.get());
        List<Score> scores = scoreDAO.findByTextId(text.getId());
        scores.sort((o1, o2) -> -new Double(o1.getScore()).compareTo(o2.getScore()));
        return new ResultView(text, scores);
    }

    private Text findSafely(long id) {
        return textsDAO.findById(id).orElseThrow(() -> new NotFoundException("No such text."));
    }

}