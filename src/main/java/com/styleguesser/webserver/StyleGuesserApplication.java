package com.styleguesser.webserver;

import com.styleguesser.webserver.core.*;
import com.styleguesser.webserver.db.ScoreDAO;
import com.styleguesser.webserver.db.TextDAO;
import com.styleguesser.webserver.db.WriterDAO;
import com.styleguesser.webserver.resources.*;
import io.dropwizard.Application;
import io.dropwizard.assets.AssetsBundle;
import io.dropwizard.auth.AuthValueFactoryProvider;
import io.dropwizard.configuration.EnvironmentVariableSubstitutor;
import io.dropwizard.configuration.SubstitutingSourceProvider;
import io.dropwizard.db.DataSourceFactory;
import io.dropwizard.hibernate.HibernateBundle;
import io.dropwizard.migrations.MigrationsBundle;
import io.dropwizard.setup.Bootstrap;
import io.dropwizard.setup.Environment;
import io.dropwizard.views.ViewBundle;
import org.glassfish.jersey.server.filter.RolesAllowedDynamicFeature;

import java.util.Map;

public class StyleGuesserApplication extends Application<StyleGuesserConfiguration> {
    public static void main(String[] args) throws Exception {
        new StyleGuesserApplication().run(args);
    }

    private final HibernateBundle<StyleGuesserConfiguration> hibernateBundle =
        new HibernateBundle<StyleGuesserConfiguration>(Writer.class, Text.class, Score.class) {
            @Override
            public DataSourceFactory getDataSourceFactory(StyleGuesserConfiguration configuration) {
                return configuration.getDataSourceFactory();
            }
        };

    @Override
    public String getName() {
        return "style-guesser";
    }

    @Override
    public void initialize(Bootstrap<StyleGuesserConfiguration> bootstrap) {
        // Enable variable substitution with environment variables
        bootstrap.setConfigurationSourceProvider(
                new SubstitutingSourceProvider(
                        bootstrap.getConfigurationSourceProvider(),
                        new EnvironmentVariableSubstitutor(false)
                )
        );

        bootstrap.addBundle(new AssetsBundle());
        bootstrap.addBundle(new MigrationsBundle<StyleGuesserConfiguration>() {
            @Override
            public DataSourceFactory getDataSourceFactory(StyleGuesserConfiguration configuration) {
                return configuration.getDataSourceFactory();
            }
        });
        bootstrap.addBundle(hibernateBundle);
        bootstrap.addBundle(new ViewBundle<StyleGuesserConfiguration>() {
            @Override
            public Map<String, Map<String, String>> getViewConfiguration(StyleGuesserConfiguration configuration) {
                return configuration.getViewRendererConfiguration();
            }
        });
    }

    @Override
    public void run(StyleGuesserConfiguration configuration, Environment environment) {
        final WriterDAO wdao = new WriterDAO(hibernateBundle.getSessionFactory());
        final TextDAO tdao = new TextDAO(hibernateBundle.getSessionFactory());
        final ScoreDAO sdao = new ScoreDAO(hibernateBundle.getSessionFactory());
        environment.jersey().register(new AuthValueFactoryProvider.Binder<>(User.class));
        environment.jersey().register(RolesAllowedDynamicFeature.class);
        environment.jersey().register(new WritersResource(wdao));
        environment.jersey().register(new TextResource(tdao, sdao));
    }
}
