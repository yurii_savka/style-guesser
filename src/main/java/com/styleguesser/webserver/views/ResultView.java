package com.styleguesser.webserver.views;

import com.styleguesser.webserver.core.Score;
import com.styleguesser.webserver.core.Text;
import io.dropwizard.views.View;

import java.io.IOException;
import java.util.List;

public class ResultView extends View {
    private final Text text;
    private final List<Score> scores;

    public ResultView(Text text, List<Score> scores) throws IOException {
        super("mustache/text-view.mustache");
        this.text = text;
        this.scores = scores;
    }

    public Text getText() {
        return text;
    }
    public List<Score> getScores() {
        return scores;
    }
}
