package com.styleguesser.webserver.views;

import io.dropwizard.views.View;

public class TextFormView extends View {
    public TextFormView() {
        super("mustache/text-form.mustache");
    }
}
