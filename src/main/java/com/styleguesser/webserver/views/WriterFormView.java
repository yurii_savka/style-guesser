package com.styleguesser.webserver.views;

import io.dropwizard.views.View;

public class WriterFormView extends View {
    public WriterFormView() {
        super("mustache/writer-form.mustache");
    }
}
