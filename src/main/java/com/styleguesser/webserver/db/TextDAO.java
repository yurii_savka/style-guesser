package com.styleguesser.webserver.db;

import com.styleguesser.webserver.core.Text;
import io.dropwizard.hibernate.AbstractDAO;
import org.hibernate.SessionFactory;

import java.util.List;
import java.util.Optional;

public class TextDAO extends AbstractDAO<Text> {
    public TextDAO(SessionFactory factory) {
        super(factory);
    }

    public Optional<Text> findById(Long id) {
        return Optional.ofNullable(get(id));
    }

    public Text create(Text Text) {
        return persist(Text);
    }

    public List<Text> findAll() {
        return list(namedQuery("com.style-guesser.webserver.core.Text.findAll"));
    }
}
