package com.styleguesser.webserver.db;

import com.styleguesser.webserver.core.Writer;
import io.dropwizard.hibernate.AbstractDAO;
import org.hibernate.SessionFactory;

import java.util.List;
import java.util.Optional;

public class WriterDAO extends AbstractDAO<Writer> {
    public WriterDAO(SessionFactory factory) {
        super(factory);
    }

    public Optional<Writer> findById(Long id) {
        return Optional.ofNullable(get(id));
    }

    public Writer create(Writer Writer) {
        return persist(Writer);
    }

    public List<Writer> findAll() {
        return list(namedQuery("com.style-guesser.webserver.core.Writer.findAll"));
    }
}
