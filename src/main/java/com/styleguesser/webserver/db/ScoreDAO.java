package com.styleguesser.webserver.db;

import com.styleguesser.webserver.core.Score;
import io.dropwizard.hibernate.AbstractDAO;
import org.hibernate.SessionFactory;

import java.util.List;
import java.util.Optional;

public class ScoreDAO extends AbstractDAO<Score> {
    public ScoreDAO(SessionFactory factory) {
        super(factory);
    }

    public Optional<Score> findById(Long id) {
        return Optional.ofNullable(get(id));
    }

    public Score create(Score score) {
        return persist(score);
    }

    public List<Score> findAll() {
        return list(namedQuery("com.style-guesser.webserver.core.Score.findAll"));
    }
    public List<Score> findByTextId(long textId) {
        return list(namedQuery("com.style-guesser.webserver.core.Score.findByTextId").setParameter("textId", textId));
    }
}
