package com.styleguesser.webserver.db;

import com.styleguesser.webserver.core.Score;
import io.dropwizard.testing.junit.DAOTestRule;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

public class ScoreDAOTest {

    @Rule
    public DAOTestRule daoTestRule = DAOTestRule.newBuilder()
        .addEntityClass(Score.class)
        .build();

    private ScoreDAO ScoreDAO;

    @Before
    public void setUp() throws Exception {
        ScoreDAO = new ScoreDAO(daoTestRule.getSessionFactory());
    }

    @Test
    public void createScore() {
        final Score score = daoTestRule.inTransaction(() -> ScoreDAO.create(new Score("Jeff", 112, 0.55)));
        assertThat(score.getId()).isGreaterThan(0);
        assertThat(score.getAuthorKey()).isEqualTo("Jeff");
        assertThat(score.getTextId()).isEqualTo(112);
        assertThat(score.getScore()).isEqualTo(0.55);
        assertThat(ScoreDAO.findById(score.getId())).isEqualTo(Optional.of(score));
    }

    @Test
    public void findAll() {
        daoTestRule.inTransaction(() -> {
            ScoreDAO.create(new Score("Jeff", 112, 0.55));
            ScoreDAO.create(new Score("Jim", 113, 0.56));
            ScoreDAO.create(new Score("Randy", 114, 0.57));
        });

        final List<Score> Scores = ScoreDAO.findAll();
        assertThat(Scores).extracting("authorKey").containsOnly("Jeff", "Jim", "Randy");
        assertThat(Scores).extracting("textId").containsOnly(112L, 113L, 114L);
        assertThat(Scores).extracting("score").containsOnly(0.55, 0.56, 0.57);
    }

    @Test
    public void findByTextId() {
        daoTestRule.inTransaction(() -> {
            ScoreDAO.create(new Score("Jeff", 112, 0.55));
            ScoreDAO.create(new Score("Jim", 112, 0.56));
            ScoreDAO.create(new Score("Randy", 114, 0.57));
        });

        final List<Score> Scores = ScoreDAO.findByTextId(112);
        assertThat(Scores).extracting("authorKey").containsOnly("Jeff", "Jim");
        assertThat(Scores).extracting("textId").containsOnly(112L);
        assertThat(Scores).extracting("score").containsOnly(0.55, 0.56);
    }

}
