package com.styleguesser.webserver.db;

import com.styleguesser.webserver.core.Writer;
import io.dropwizard.testing.junit.DAOTestRule;
import org.hibernate.exception.ConstraintViolationException;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

public class WriterDAOTest {

    @Rule
    public DAOTestRule daoTestRule = DAOTestRule.newBuilder()
        .addEntityClass(Writer.class)
        .build();

    private WriterDAO WriterDAO;

    @Before
    public void setUp() throws Exception {
        WriterDAO = new WriterDAO(daoTestRule.getSessionFactory());
    }

    @Test
    public void createWriter() {
        final Writer jeff = daoTestRule.inTransaction(() -> WriterDAO.create(new Writer("Jeff", "Postmodern poet")));
        assertThat(jeff.getId()).isGreaterThan(0);
        assertThat(jeff.getFullName()).isEqualTo("Jeff");
        assertThat(jeff.getDescription()).isEqualTo("Postmodern poet");
        assertThat(WriterDAO.findById(jeff.getId())).isEqualTo(Optional.of(jeff));
    }

    @Test
    public void findAll() {
        daoTestRule.inTransaction(() -> {
            WriterDAO.create(new Writer("Jeff", "Postmodern poet"));
            WriterDAO.create(new Writer("Jim", "Surrealist rapper"));
            WriterDAO.create(new Writer("Randy", "True classic"));
        });

        final List<Writer> Writers = WriterDAO.findAll();
        assertThat(Writers).extracting("fullName").containsOnly("Jeff", "Jim", "Randy");
        assertThat(Writers).extracting("description").containsOnly("Postmodern poet", "Surrealist rapper", "True classic");
    }

    @Test(expected = ConstraintViolationException.class)
    public void handlesNullFullName() {
        daoTestRule.inTransaction(() -> WriterDAO.create(new Writer(null, "The null")));
    }
}
