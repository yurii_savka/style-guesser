package com.styleguesser.webserver.db;

import com.styleguesser.webserver.core.Text;
import io.dropwizard.testing.junit.DAOTestRule;
import org.hibernate.exception.ConstraintViolationException;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;

import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;

public class TextDAOTest {

    @Rule
    public DAOTestRule daoTestRule = DAOTestRule.newBuilder()
        .addEntityClass(Text.class)
        .build();

    private TextDAO TextDAO;

    @Before
    public void setUp() throws Exception {
        TextDAO = new TextDAO(daoTestRule.getSessionFactory());
    }

    @Test
    public void createText() {
        final Text jeff = daoTestRule.inTransaction(() -> TextDAO.create(new Text("This is content")));
        assertThat(jeff.getId()).isGreaterThan(0);
        assertThat(jeff.getContent()).isEqualTo("This is content");
        assertThat(TextDAO.findById(jeff.getId())).isEqualTo(Optional.of(jeff));
    }

    @Test
    public void findAll() {
        daoTestRule.inTransaction(() -> {
            TextDAO.create(new Text("Cool poem"));
            TextDAO.create(new Text("Nice novel"));
            TextDAO.create(new Text("Disgusting feuilleton"));
        });

        final List<Text> Texts = TextDAO.findAll();
        assertThat(Texts).extracting("content").containsOnly("Cool poem", "Nice novel", "Disgusting feuilleton");
    }

    @Test(expected = ConstraintViolationException.class)
    public void handlesNullContent() {
        daoTestRule.inTransaction(() -> TextDAO.create(new Text(null)));
    }
}
